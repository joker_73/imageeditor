import UIKit


typealias ImageProcessorPercentHandler = (_ progress: Float) -> Void
typealias ImageProcessorCompletionHandler = (_ result: UIImage?, _ error: Error?) -> Void


protocol ImageProcessor {
    func process(image: UIImage, percent: ImageProcessorPercentHandler?,
            completion: ImageProcessorCompletionHandler?)
}


extension ImageProcessor {
    func process(percent: ImageProcessorPercentHandler? = nil,
            completion: ImageProcessorCompletionHandler? = nil) {
        process(percent: percent, completion: completion)
    }
}


// MARK: - ImageProcessorBase
class ImageProcessorBase: ImageProcessor {
    func process(image: UIImage, percent: ImageProcessorPercentHandler?,
            completion: ImageProcessorCompletionHandler?) {
        var processedImage: UIImage?
        var error: Error?

        let dispatchGroup = DispatchGroup()
        DispatchQueue.global(qos: .background).async(group: dispatchGroup) { [weak self] in
            processedImage = self?.process(image: image, error: &error)
        }

        startFakeDelay(percent, dispatchGroup)

        dispatchGroup.notify(queue: .main) {
            completion?(processedImage, error)
        }
    }

    func process(image: UIImage, error: inout Error?) -> UIImage? {
        return nil
    }

    private func startFakeDelay(_ percent: ImageProcessorPercentHandler?, _ group: DispatchGroup) {
        group.enter()

//        let delay = Double(arc4random_uniform(26) + 5) / 100
        let delay = Double(arc4random_uniform(4)) / 100

        var progress: Float = 0

        let timer = DispatchSource.makeTimerSource()
        timer.schedule(deadline: .now(), repeating: delay)
        timer.setEventHandler {
            DispatchQueue.main.async {
                percent?(progress)
            }

            progress += 0.01

            if progress > 1 {
                timer.cancel()
                timer.setEventHandler(handler: nil)

                group.leave()
            }
        }
        timer.resume()
    }
}
