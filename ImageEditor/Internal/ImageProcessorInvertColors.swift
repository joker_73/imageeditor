import UIKit


final class ImageProcessorInvertColors: ImageProcessorBase {
    override func process(image: UIImage, error: inout Error?) -> UIImage? {
        let filter = CIFilter(name: "CIColorInvert")!
        let ciImage = image.ciImage ?? CIImage(image: image)!
        filter.setValue(ciImage, forKey: kCIInputImageKey)
        let result = filter.outputImage!
        return UIImage(ciImage: result, scale: image.scale, orientation: image.imageOrientation)
    }
}
