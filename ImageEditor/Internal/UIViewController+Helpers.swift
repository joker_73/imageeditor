import UIKit
import Photos


extension UIViewController {
    func openAppSettings() {
        if let url = URL(string: UIApplicationOpenSettingsURLString),
                UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
