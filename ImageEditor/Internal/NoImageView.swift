import UIKit


@IBDesignable
final class NoImageView: UIView {
    @IBInspectable var line: UIColor = UIColor(hex: 0xE6E6E6)
    @IBInspectable var background: UIColor = UIColor(hex: 0xF2F6F9)
    @IBInspectable var lineWidth: Float = 2
    @IBInspectable var cornerRadius: Float = 10


    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }

        let lineWidth = CGFloat(self.lineWidth)
        let cornerRadius = CGFloat(self.cornerRadius)

        context.setFillColor(background.cgColor)
        context.setStrokeColor(line.cgColor)
        context.setLineWidth(lineWidth)

        let path = UIBezierPath(roundedRect: rect, cornerRadius: cornerRadius).cgPath
        context.addPath(path)
        context.fillPath()
        context.addPath(path)
        context.strokePath()

        context.move(to: CGPoint(x: rect.minX + cornerRadius, y: rect.minY + cornerRadius))
        context.addLine(to: CGPoint(x: rect.maxX - cornerRadius, y: rect.maxY - cornerRadius))
        context.move(to: CGPoint(x: rect.maxX - cornerRadius, y: rect.minY + cornerRadius))
        context.addLine(to: CGPoint(x: rect.minX + cornerRadius, y: rect.maxY - cornerRadius))
        context.strokePath()
    }
}
