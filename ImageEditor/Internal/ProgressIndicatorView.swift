import UIKit


@IBDesignable
final class ProgressIndicatorView: UIView {
    @IBOutlet private weak var view: UIView!
    @IBOutlet private weak var progressView: UIProgressView!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
}


// MARK: -
extension ProgressIndicatorView {
    func set(progress: Float) {
        activityIndicator.stopAnimating()
        progressView.progress = progress
        progressView.isHidden = false
    }

    func reset() {
        progressView.progress = 0
        progressView.isHidden = true
        activityIndicator.startAnimating()
    }
}


// MARK: - Private
private extension ProgressIndicatorView {
    func setup() {
        backgroundColor = .clear

        view = loadViewFromNib()
        addRoot(view: view)

        reset()
    }
}
