import UIKit


@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?


    func application(_ application: UIApplication,
            didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow()

        let imageEditorViewController = UIStoryboard.init(name: "ImageEditor", bundle: nil)
                .instantiateInitialViewController()
        window?.rootViewController = imageEditorViewController
        window?.makeKeyAndVisible()

        return true
    }
}

