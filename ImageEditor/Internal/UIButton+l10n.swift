import UIKit


//@IBDesignable
extension UIButton {
    @IBInspectable
    var localizedTitleForNormal: String {
        get {
            return title(for: .normal)!
        }

        set(key) {
            setTitle(key.lc, for: .normal)
        }
    }

    @IBInspectable
    var localizedTitleForHighlighted: String {
        get {
            return title(for: .highlighted)!
        }

        set(key) {
            setTitle(key.lc, for: .highlighted)
        }
    }
}
