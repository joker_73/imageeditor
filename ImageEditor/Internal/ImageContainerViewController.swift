import UIKit
import Photos


typealias ImagesContainerProgressHandler = (Float) -> Void
typealias ImagesContainerCompletionHandler = (UIImage?, Error?) -> Void


protocol ImagesContainerItem: ImagesContainerCellData {
    func process(progress: @escaping ImagesContainerProgressHandler,
            completion: @escaping ImagesContainerCompletionHandler)
}


protocol ImagesContainer {
    func append(item: ImagesContainerItem)
}


protocol ImagesContainerDelegate: AnyObject {
    func imageWillBeProcessed(in imagesContainer: ImagesContainer)
    func imagesContainer(_ imagesContainer: ImagesContainer, progress: Float)
    func imageDidBeProcessed(in imagesContainer: ImagesContainer)
    func imagesContainer(_ imagesContainer: ImagesContainer, didSelect image: UIImage)
}


private let kCellIdentifier = "imagesContainer"
private let kAlbumName = "Image Editor"


final class ImagesContainerViewController: UIViewController {
    weak var delegate: ImagesContainerDelegate?

    private var items: [ImagesContainerItem] = []
    private var imageInProcess: Bool = false

    @IBOutlet private weak var listView: UICollectionView!
}


// MARK: - Private
private extension ImagesContainerViewController {
    func removeItem(at indexPath: IndexPath) {
        items.remove(at: indexPath.row)
        listView.performBatchUpdates({ [unowned self] in
            self.listView.deleteItems(at: [indexPath])
        })
    }

    func store(image: UIImage) {
        PHPhotoLibrary.perform(accessGranted: {
#if false
            let imageData = UIImagePNGRepresentation(image.checked())
            let compressedImage = UIImage(data: imageData!)
            UIImageWriteToSavedPhotosAlbum(compressedImage!, nil, nil, nil)
#else
            PHPhotoLibrary.store(image: image, albumName: kAlbumName)
#endif
        }, accessDenied: { [unowned self] in
            self.openAppSettings()
        })
    }

    func process(item: ImagesContainerItem, completion: @escaping (UIImage) -> Void) {
        guard !imageInProcess else {
            return
        }

        imageInProcess = true
        delegate?.imageWillBeProcessed(in: self)

        item.process(progress: { [unowned self] in
            self.delegate?.imagesContainer(self, progress: $0)
        }, completion: { [unowned self] image, error in
            self.imageInProcess = false
            self.delegate?.imageDidBeProcessed(in: self)

            if let image = image, case .none = error {
                completion(image)
            } else {
                // TODO: Handle error
                print("[Warning] Process an image: an error has occurred")
            }
        })
    }

    func selectProcessedImage(from item: ImagesContainerItem) {
        process(item: item) { [unowned self] image in
            self.delegate?.imagesContainer(self, didSelect: image)
        }
    }

    func storeProcessedImage(from item: ImagesContainerItem) {
        process(item: item) { [unowned self] image in
            self.store(image: image)
        }
    }
}


// MARK: - ImagesContainer
extension ImagesContainerViewController: ImagesContainer {
    func append(item: ImagesContainerItem) {
        items.insert(item, at: 0)
        listView.performBatchUpdates({ [unowned self] in
            self.listView.insertItems(at: [IndexPath(row: 0, section: 0)])
        })
    }
}


// MARK: - UICollectionViewDataSource
extension ImagesContainerViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView,
            numberOfItemsInSection section: Int) -> Int {
        return items.count
    }

    func collectionView(_ collectionView: UICollectionView,
            cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return listView.dequeueReusableCell(withReuseIdentifier: kCellIdentifier, for: indexPath)
    }
}


// MARK: - UICollectionViewDelegate
extension ImagesContainerViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell,
            forItemAt indexPath: IndexPath) {
        if let cell = cell as? ImagesContainerCell {
            cell.data = items[indexPath.row]
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let alert = UIAlertController(title: nil, message: "Images container.Alert.Message".lc,
                preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Images container.Alert.Button.Store".lc,
                style: .default) { [unowned self] _ in
            let item = self.items[indexPath.row]
            self.storeProcessedImage(from: item)
        })

        alert.addAction(UIAlertAction(title: "Image provider.Alert.Button.Remove".lc,
                style: .default) { [unowned self] _ in
            self.removeItem(at: indexPath)
        })

        alert.addAction(UIAlertAction(title: "Image provider.Alert.Button.Select".lc,
                style: .default) { [unowned self] _ in
            let item = self.items[indexPath.row]
            self.selectProcessedImage(from: item)
        })

        present(alert, animated: true)
    }
}
