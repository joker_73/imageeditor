import UIKit


final class ImageProcessorRotate: ImageProcessorBase {
    var angle: CGFloat = .pi / 2


    override func process(image: UIImage, error: inout Error?) -> UIImage? {
        let size = image.size
        let hasAlpha = false
        let scale: CGFloat = 0.0

        UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
        let context = UIGraphicsGetCurrentContext()!

        let matrix = CGAffineTransform.identity
                .translatedBy(x: 0.5 * size.width, y: 0.5 * size.height)
                .rotated(by: angle)
                .translatedBy(x: -0.5 * size.width, y: -0.5 * size.height)
        context.concatenate(matrix)

        image.draw(at: CGPoint.zero)

        let resultImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return resultImage
    }
}
