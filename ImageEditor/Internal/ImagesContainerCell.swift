import UIKit


typealias ImagesContainerCellProgressListener = (_ progress: Float?) -> Void
typealias ImagesContainerCellCompleteListener = () -> Void


protocol ImagesContainerCellData: AnyObject {
    var preview: UIImage? { get }
    var progress: Float? { get }
    var progressListener: ImagesContainerCellProgressListener? { get set }
    var completeListener: ImagesContainerCellCompleteListener? { get set }
}


final class ImagesContainerCell: UICollectionViewCell {
    var data: ImagesContainerCellData? {
        willSet {
            data?.progressListener = nil
            data?.completeListener = nil
            set(progress: nil)
            set(image: nil, animated: false)
            progressContainerView.alpha = 1
            progressContainerView.isHidden = false
        }
        didSet {
            guard let data = data else { return }
            set(progress: data.progress)
            set(image: data.preview, animated: false)
            data.progressListener = { [weak self] in self?.set(progress: $0) }
            data.completeListener = { [weak self] in self?.set(image: data.preview) }
        }
    }

    @IBOutlet private weak var imageContainerView: ImageContainerView!
    @IBOutlet private weak var progressContainerView: UIView!
    @IBOutlet private weak var progressIndicatorView: ProgressIndicatorView!
}


// MARK: - Lifecycle
extension ImagesContainerCell {
    override func prepareForReuse() {
        super.prepareForReuse()

        data = nil
    }
}


// MARK: - Private
private extension ImagesContainerCell {
    func set(image: UIImage?, animated: Bool = true) {
        imageContainerView.image = image

        let isReady = image == .none ? false : true
        guard progressContainerView.isHidden != isReady else {
            return
        }

        if animated {
            let options: UIViewAnimationOptions = [isReady ? .curveEaseIn : .curveEaseOut]
            progressContainerView.alpha = isReady ? 1 : 0
            UIView.animate(withDuration: 0.7, delay: 0, options: options, animations: {
                self.progressContainerView.alpha = isReady ? 0 : 1
            }, completion: { _ in
                self.progressContainerView.isHidden = isReady
            })
        } else {
            progressContainerView.isHidden = isReady
        }
    }

    func set(progress: Float?) {
        if let progress = progress {
            progressIndicatorView.set(progress: progress)
        } else {
            progressIndicatorView.reset()
        }
    }
}
