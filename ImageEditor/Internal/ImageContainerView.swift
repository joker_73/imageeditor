import UIKit


@IBDesignable
final class ImageContainerView: UIView {
    @IBInspectable var cornerRadius: Float = 10 {
        didSet {
            configure()
        }
    }

    var image: UIImage? {
        get {
            return imageView.image
        }
        set {
            imageView.image = newValue
        }
    }

    @IBOutlet private weak var view: UIView!
    @IBOutlet private weak var container: UIView!
    @IBOutlet private weak var noImageView: NoImageView!
    @IBOutlet private weak var imageView: UIImageView!


    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
}


// MARK: - Private
private extension ImageContainerView {
    func setup() {
        view = loadViewFromNib()
        addRoot(view: view)

        configure()
    }

    func configure() {
        view.layer.masksToBounds = false
        view.layer.cornerRadius = CGFloat(cornerRadius)
        view.layer.shadowOpacity = 0.3
        view.layer.shadowRadius = 8
        view.layer.shadowOffset = CGSize(width: 2, height: 2)

        container.layer.masksToBounds = true
        container.layer.cornerRadius = CGFloat(cornerRadius)

        noImageView.cornerRadius = cornerRadius
    }
}
