import Photos


typealias PhotoLibraryAccessGrantedAction = () -> Void
typealias PhotoLibraryAccessDeniedAction = () -> Void


extension PHPhotoLibrary {
    static func perform(accessGranted: @escaping PhotoLibraryAccessGrantedAction,
            accessDenied: PhotoLibraryAccessDeniedAction? = nil) {
        switch PHPhotoLibrary.authorizationStatus() {
        case .restricted, .denied:
            accessDenied?()

        case .notDetermined:
            PHPhotoLibrary.requestAuthorization { status in
                if case .authorized = status {
                    accessGranted()
                }
            }

        case .authorized:
            accessGranted()
        }
    }

    static func store(image: UIImage, albumName: String) {
        album(with: albumName) { album in
            guard let album = album else {
                // TODO: Handle error
                return
            }

            PHPhotoLibrary.shared().performChanges({
                let assetRequest = PHAssetChangeRequest
                        .creationRequestForAsset(from: image.checked())

                let albumChangeRequest = PHAssetCollectionChangeRequest(for: album)

                if let assetPlaceholder = assetRequest.placeholderForCreatedAsset {
                    let assetPlaceholders: NSArray = [assetPlaceholder]
                    albumChangeRequest?.addAssets(assetPlaceholders)
                }
            }, completionHandler: { success, error in
                guard !success else { return }
                // TODO: Handle error
                print("[Warning] Store an image: an error has occurred (\(error?.localizedDescription ?? "-"))")
            })
        }
    }

    static func album(with name: String, completion: @escaping (PHAssetCollection?) -> Void) {
        let options = PHFetchOptions()
        options.predicate = NSPredicate(format: "title = %@", name)

        let collection = PHAssetCollection
                .fetchAssetCollections(with: .album, subtype: .any, options: options)

        if let album = collection.firstObject {
            completion(album)
        } else {
            create(album: name, completion: completion)
        }
    }

    static func create(album name: String, completion: @escaping (PHAssetCollection?) -> Void) {
        var placeholder: PHObjectPlaceholder?

        PHPhotoLibrary.shared().performChanges({
            let request = PHAssetCollectionChangeRequest
                    .creationRequestForAssetCollection(withTitle: name)

            placeholder = request.placeholderForCreatedAssetCollection
        }, completionHandler: { (success, error) -> Void in
            guard success, let id = placeholder?.localIdentifier else {
                completion(nil)
                return
            }

            let fetchResult = PHAssetCollection
                    .fetchAssetCollections(withLocalIdentifiers: [id], options: nil)

            completion(fetchResult.firstObject)
        })
    }
}
