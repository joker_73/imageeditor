import UIKit


final class ImageProcessorMirror: ImageProcessorBase {
    override func process(image: UIImage, error: inout Error?) -> UIImage? {
        let size = image.size
        let hasAlpha = false
        let scale: CGFloat = 0.0

        UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
        let context = UIGraphicsGetCurrentContext()!

        // https://code-industry.ru/masterpdfeditor-help/transformation-matrix/
        let matrix = CGAffineTransform(a: -1, b: 0, c: 0, d: 1, tx: size.width, ty: 0)
        context.concatenate(matrix)

        image.draw(at: CGPoint.zero)

        let resultImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return resultImage
    }
}
