import UIKit


// TODO: Add cache


final class ImagesContainerItemAdapter: ImagesContainerItem {
    var preview: UIImage? {
        didSet { completeListener?() }
    }
    var progress: Float? {
        didSet { progressListener?(progress) }
    }
    var progressListener: ImagesContainerCellProgressListener? = nil
    var completeListener: ImagesContainerCellCompleteListener? = nil

    private let originalImage: UIImage
    private let imageProcessor: ImageProcessor
    private let previewSize: CGSize


    init(previewSize: CGSize, image: UIImage, imageProcessor: ImageProcessor) {
        self.previewSize = previewSize
        originalImage = image
        self.imageProcessor = imageProcessor

        generatePreview()
    }
}


// MARK: - ImagesContainerItem
extension ImagesContainerItemAdapter {
    func process(progress: @escaping ImagesContainerProgressHandler,
            completion: @escaping ImagesContainerCompletionHandler) {
        imageProcessor.process(image: originalImage, percent: { progress($0) },
                completion: { completion($0, $1) })
    }
}


// MARK: - Private
private extension ImagesContainerItemAdapter {
    func generatePreview() {
        imageProcessor.process(image: scaledToPreviewOriginalImage(), percent: { [weak self] in
            self?.progress = $0
        }, completion: { [weak self] image, error in
            self?.preview = image

            if case .some = error {
                // TODO: Handle error
                print("[Warning] Generate a preview: an error has occurred")
            }
        })
    }

    func scaledToPreviewOriginalImage() -> UIImage {
        let minSide = CGFloat.minimum(previewSize.width, previewSize.height)
        let minImageSide = CGFloat.minimum(originalImage.size.width, originalImage.size.height)

        guard minSide < minImageSide else {
            return originalImage
        }

        let factor = minSide / minImageSide
        let size = originalImage.size.applying(CGAffineTransform(scaleX: factor, y: factor))
        let hasAlpha = false
        let scale: CGFloat = 0.0

        UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
        originalImage.draw(in: CGRect(origin: CGPoint.zero, size: size))

        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return scaledImage!
    }
}
