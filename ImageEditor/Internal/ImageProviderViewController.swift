import UIKit
import AVFoundation
import Photos


protocol ImageProvider {
    var image: UIImage? { get set }
}


protocol ImageProviderDelegate: AnyObject {
    func imageProvider(_ imageProvider: ImageProvider, didLoad image: UIImage?)
}


private enum ImageProviderPickerMode {
    case camera
    case library
}


final class ImageProviderViewController: UIViewController {
    weak var delegate: ImageProviderDelegate?

    // MARK: ImageProvider
    var image: UIImage? {
        get {
            if isViewLoaded {
                return imageContainerView.image
            } else {
                return nil
            }
        }
        set {
            if isViewLoaded {
                set(image: newValue, notify: false)
            }
        }
    }

    @IBOutlet private weak var imageContainerView: ImageContainerView!
    @IBOutlet private weak var titleLabel: UILabel!
}


// MARK: - Lifecycle
extension ImageProviderViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        updateView()
    }
}


// MARK: - Actions
private extension ImageProviderViewController {
    @IBAction func tap() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let alert = UIAlertController(title: "Image provider.Alert.Title".lc,
                    message: "Image provider.Alert.Message".lc, preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "Image provider.Alert.Button.Camera".lc,
                    style: .default) { [weak self] _ in
                self?.takePhoto()
            })

            alert.addAction(UIAlertAction(title: "Image provider.Alert.Button.Library".lc,
                    style: .default) { [weak self] _ in
                self?.loadPhoto()
            })

            present(alert, animated: true)
        } else {
            loadPhoto()
        }
    }
}


// MARK: - ImageProvider
extension ImageProviderViewController: ImageProvider {

}


// MARK: - Private
private extension ImageProviderViewController {
    func configureView() {
        titleLabel.text = "Image provider.Title".lc
    }

    func takePhoto() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .restricted, .denied:
            openAppSettings()

        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { [weak self] allowed in
                if allowed { self?.showPicker(for: .camera) }
            }

        case .authorized:
            showPicker(for: .camera)
        }
    }

    func loadPhoto() {
        PHPhotoLibrary.perform(accessGranted: { [unowned self] in
            self.showPicker(for: .library)
        }, accessDenied: { [unowned self] in
            self.openAppSettings()
        })
    }

    func showPicker(for mode: ImageProviderPickerMode) {
        let picker = UIImagePickerController()
        // Causes a crash when the parent view controller isn't full-screen
        // picker.modalTransitionStyle = .partialCurl
        picker.modalPresentationStyle = .overCurrentContext
        picker.delegate = self

        switch mode {
        case .camera:
            picker.sourceType = .camera

        case .library:
            picker.sourceType = .photoLibrary
        }

        present(picker, animated: true)
    }

    func updateView() {
        if case .none = imageContainerView.image {
            titleLabel.isHidden = false
        } else {
            titleLabel.isHidden = true
        }
    }

    func set(image: UIImage?, notify: Bool = true) {
        imageContainerView.image = image
        updateView()

        if notify {
            delegate?.imageProvider(self, didLoad: image)
        }
    }
}


// MARK: - UIImagePickerControllerDelegate
extension ImageProviderViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController,
            didFinishPickingMediaWithInfo info: [String: Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            set(image: image)
            picker.dismiss(animated: true)
        }
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
}


// MARK: - UIImagePickerControllerDelegate
extension ImageProviderViewController: UINavigationControllerDelegate {

}
