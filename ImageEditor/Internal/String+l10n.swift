import Foundation


private final class Marker {

}


extension String {
    var lc: String {
        let bundle = Bundle(for: Marker.self)
        return NSLocalizedString(self, bundle: bundle, comment: "")
    }

    func lc(_ args: CVarArg...) -> String {
        return withVaList(args) {
            NSString(format: self.lc, locale: NSLocale.current, arguments: $0)
        } as String
    }
}
