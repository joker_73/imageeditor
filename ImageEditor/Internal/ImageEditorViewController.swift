import UIKit


final class ImageEditorViewController: UIViewController {
    private var imageProvider: ImageProvider!
    private var imagesContainer: ImagesContainer!

    @IBOutlet private var buttons: [UIButton]!
    @IBOutlet private weak var progressContainerView: UIVisualEffectView!
    @IBOutlet private weak var progressIndicatorView: ProgressIndicatorView!
}


private let kCellSize = CGSize(width: 136, height: 136)


// MARK: - Lifecycle
extension ImageEditorViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        configureViews()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch (segue.identifier, segue.destination) {
        case (.some("imageProvider"), let viewController as ImageProviderViewController):
            viewController.delegate = self
            imageProvider = viewController
            setButtons(state: imageProvider.image != .none)

        case (.some("imagesContainer"), let viewController as ImagesContainerViewController):
            viewController.delegate = self
            imagesContainer = viewController

        default:
            break
        }
    }
}


// MARK: - Actions
extension ImageEditorViewController {
    @IBAction func rotateButtonTap() {
        if let image = imageProvider.image {
            let item = ImagesContainerItemAdapter(previewSize: kCellSize, image: image,
                    imageProcessor: ImageProcessorRotate())
            imagesContainer.append(item: item)
        }
    }

    @IBAction func invertColorsButtonTap() {
        if let image = imageProvider.image {
            let item = ImagesContainerItemAdapter(previewSize: kCellSize, image: image,
                    imageProcessor: ImageProcessorInvertColors())
            imagesContainer.append(item: item)
        }
    }

    @IBAction func mirrorImageButtonTap() {
        if let image = imageProvider.image {
            let item = ImagesContainerItemAdapter(previewSize: kCellSize, image: image,
                    imageProcessor: ImageProcessorMirror())
            imagesContainer.append(item: item)
        }
    }
}


// MARK: - Private
private extension ImageEditorViewController {
    func configureViews() {
        buttons.forEach { $0.contentHorizontalAlignment = .left }
        buttons.flatMap { $0.titleLabel }.forEach { label in
            label.lineBreakMode = .byTruncatingTail
            label.numberOfLines = 2
            label.minimumScaleFactor = 0.8
            label.adjustsFontSizeToFitWidth = true
        }

        hideProgress(animated: false)
    }

    func setButtons(state: Bool) {
        buttons.forEach { $0.isEnabled = state }
    }

    func showProgress() {
        progressIndicatorView.reset()
        progressContainerView.isHidden = false
        UIView.animate(withDuration: 0.7, delay: 0, options: [.curveEaseOut], animations: {
            self.progressIndicatorView.alpha = 1
            self.progressContainerView.effect = UIBlurEffect(style: .light)
        })
    }

    func hideProgress(animated: Bool = true) {
        if animated {
            UIView.animate(withDuration: 0.7, delay: 0, options: [.curveEaseIn], animations: {
                self.progressIndicatorView.alpha = 0
                self.progressContainerView.effect = nil
            }, completion: { _ in
                self.progressContainerView.isHidden = true
            })
        } else {
            self.progressIndicatorView.alpha = 0
            progressContainerView.effect = nil
            progressContainerView.isHidden = true
        }
    }
}


// MARK: - ImageProviderDelegate
extension ImageEditorViewController: ImageProviderDelegate {
    func imageProvider(_ imageProvider: ImageProvider, didLoad image: UIImage?) {
        setButtons(state: image != .none)
    }
}


// MARK: - ImagesContainerDelegate
extension ImageEditorViewController: ImagesContainerDelegate {
    func imageWillBeProcessed(in imagesContainer: ImagesContainer) {
        showProgress()
    }

    func imagesContainer(_ imagesContainer: ImagesContainer, progress: Float) {
        progressIndicatorView.set(progress: progress)
    }

    func imageDidBeProcessed(in imagesContainer: ImagesContainer) {
        hideProgress()
    }

    func imagesContainer(_ imagesContainer: ImagesContainer, didSelect image: UIImage) {
        imageProvider.image = image
    }
}
